'use strict';

var path        = require('path');
var fs          = require('fs');
var Handlebars  = require('handlebars');
var metalsmith  = require('metalsmith');
var templates   = require('metalsmith-templates');
var metasmithLess = require('metalsmith-less');
var serve = require('metalsmith-serve');
var watch = require('metalsmith-watch');
var layouts     = require('handlebars-layouts');
var navigation  = require('metalsmith-navigation');
var assets      = require('metalsmith-assets');
var multiLanguage = require('metalsmith-multi-language');
var uglify     = require('metalsmith-uglify');

/**
 * MultiLanguage
 */
var langConfig = {
    default: 'ru',
    locales: ['en','ru']
};
var langTask = multiLanguage(langConfig);

/**
 * Navigation
 */


var navConfigs = {
    primary:{
        sortBy: 'nav_sort',
        filterProperty: 'nav_groups'
    },
    shopper: {
        sortBy: 'nav_sort',
        filterProperty: 'nav_groups',
        includeDirs: true
    },
    client: {
        sortBy: 'nav_sort',
        filterProperty: 'nav_groups',
        includeDirs: true
    },
    clientSub: {
        sortBy: 'nav_sort',
        filterProperty: 'nav_groups',
        includeDirs: true
    }
};

var navSettings = {
    navListProperty: 'navs'
};

var navTask = navigation(navConfigs, navSettings);

/**
 * Theme & Templates
 */
var themePath = './themes/default';

var assetsTask = assets({
    source: themePath + '/assets',
    destination: './assets'
});

var templatesTask = templates({
    engine: 'handlebars',
    directory: themePath + '/templates'
});
/**
 * less
 */
var lessConfig = {
    render: {
        compress: true
    }
};
var less = metasmithLess(lessConfig);

/**
 * Handlebars heplers
 */

var relativePathHelper = function(current, target) {
    // normalize and remove starting slash from path
    if(!current || !target){
        return '';
    }
    current = path.normalize(current).slice(0);
    target = path.normalize(target).slice(0);
    current = path.dirname(current);
    return path.relative(current, target);
};
var isActiveHelper = function(current, item, where){
    if(!current || !item || !where){
        return '';
    }
    else if (current.localeCompare(item) === 0){
        if (where.localeCompare("largeMenu")===0) return 'selected active';//return 'sibling';
        else if (where.localeCompare("largeMenuText")===0) return 'class=activePage';
        else if (where.localeCompare("subMenuText")===0) return 'active';
    }
    else if(where.localeCompare("largeMenu")===0) return 'sibling';
};
Handlebars.registerHelper('relative_path', relativePathHelper);
Handlebars.registerHelper('isActive', isActiveHelper);
Handlebars.registerHelper(layouts(Handlebars));
Handlebars.registerPartial('layout', fs.readFileSync(themePath + '/templates/layout.html', 'utf8'));


var metalsmith = metalsmith(__dirname)
    .use(watch({
        paths: {
            "${source}/**/*": true,
            "templates/**/*": true
        },
        livereload: true
    })
)
    .use(serve())
    .clean(true)
    .use(navTask)
    .use(less)
    .use(templatesTask)
    .use(assetsTask)
    .use(langTask)
    .use(uglify())
    .build(function(err) {
        if (err) throw err;
    });

### Install

* Clone the repo
* Install node.js v0.12+ (version check: **node -v**)
* Run

```bash
npm i; npm start
```